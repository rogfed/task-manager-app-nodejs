const mongodb = require('mongodb');
const {MongoClient, ObjectID} = mongodb;

const connectionURL = 'mongodb://rf-admin:RF4dm1n@ds263048.mlab.com:63048/rf-task-app';

MongoClient.connect(connectionURL, {useNewUrlParser: true, useUnifiedTopology: true}, (error, client) => {
  if(error){
    return console.log('Unable to connect to DB.');
  }
  const db = client.db();

  // db.collection('users').findOne({_id: new ObjectID('5e540ee7a567f9558051649f')}, (error, user) => {
  //   if(error){
  //     return console.log('Unable to find user.')
  //   }

  //   console.log(user);
  // });

  // db.collection('users').find({age: 30}).toArray((error, users) => {
  //   console.log(users);
  // });

  // db.collection('tasks').findOne({_id: new ObjectID('5e54165237b6c6620f9494ac')}, (error, task) => {
  //   console.log(task);
  // })

  db.collection('tasks').find({completed: false}).toArray((error, tasks) => {
    console.log(tasks);
  })
});